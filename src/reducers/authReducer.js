import * as types from '../actions/actionsTypes';
import initialState from './initialState';

export const authentication = (state = initialState.authentication, action) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return {
        canEdit: action.canEdit,
        loggedIn: true,
        error: null
      };

    case types.LOGIN_ERROR:
      return {
        canEdit: false,
        loggedIn: false,
        error: action.hasOwnProperty('error') ? action.error : null
      };

    case types.LOGOUT_SUCCESS:
      return {
        canEdit: false,
        loggedIn: false,
        error: null
      };

    default:
      return state;
  }
};