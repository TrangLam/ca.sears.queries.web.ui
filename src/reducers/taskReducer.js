// import * as _ from 'lodash';
import * as types from '../actions/actionsTypes';
import initialState from './initialState';

/**
 * Reducer to update TasksList state
 * @param state
 * @param action
 * @return {*}
 */
export const tasksList = (state=initialState.tasksList, action) => {
  switch (action.type) {
    case types.LOAD_TASKS_SUCCESS:
      return {
        tasks: action.tasks,
        error: null
      };

    case types.LOAD_TASKS_FAILURE:
      return {
        ...state,
        error: action.error.message
      };

    default:
      return state;
  }
};

/**
 * Reducer to update TaskComponents state
 * @param state
 * @param action
 * @return {*}
 */
export const taskComponents = (state=initialState.taskComponents, action) => {
  switch (action.type) {
    case types.LOAD_TASK_COMPONENTS_SUCCESS:
      return action.taskComponents;

    case types.LOAD_TASK_COMPONENTS_FAILURE:
      return state;

    default:
      return state;
  }
};

/**
 * Reducer to update taskToDelete
 *
 * @param state
 * @param action
 * @return {*}
 */
export const taskToDelete = (state = initialState.taskToDelete, action) => {
  switch (action.type) {
    case types.SELECT_TO_DELETE:
      return action.task;

    case types.CLEAR_TASK_TO_DELETE:
      return {};

    default:
      return state;
  }
};

/**
 * Reducer to update deleteModal
 *
 * @param state
 * @param action
 * @return {*}
 */
export const deleteModal = (state = initialState.deleteModal, action) => {
  switch (action.type) {
    case types.TOGGLE_DELETE_MODAL:
      return {
        show: !state.show,
        error: null
      };

    case types.DELETE_TASKS_FAILURE:
      return {
        ...state,
        error: action.error.message
      };

    default:
      return state;
  }
};

/**
 * Reducer to update submitForm
 *
 * @param state
 * @param action
 * @return {*}
 */
export const submitForm = (state = initialState.submitForm, action) => {
  switch (action.type) {
    case types.SUBMIT_FORM_SUCCESS:
      return {
        error: null,
        succeeded: true
      };

    case types.SUBMIT_FORM_FAILURE:
      return {
        ...state,
        error: action.error.message
      };

    case types.RESET_SUBMIT_FORM:
      return initialState.submitForm;

    default:
      return state;
  }
};

export const addScene = (state = initialState.addScene, action) => {
  switch (action.type) {
    case types.CHOOSE_ITEM:
      return {
        ...state,
        chosenItem: action.chosenItem
      };

    case types.CHOOSE_PROFILE:
      return {
        ...state,
        chosenProfile: action.chosenProfile
      };

    case types.CLEAR_ADD_SCENE: // When AddScene unmounts
      return initialState.addScene;

    case types.CLEAR_PROFILE:
      return {
        ...state,
        chosenProfile: ''
      };

    default:
      return state;
  }
};