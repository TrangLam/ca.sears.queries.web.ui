export default {
  authentication : {
    canEdit: false,
    loggedIn: false,
    error: null
  },
  tasksList: {
    tasks: {},
    error: null,
  },
  taskComponents: {},
  taskToDelete: {},
  deleteModal: {
    show: false,
    error: null
  },
  submitForm: {
    // initialValues: {},
    succeeded: false,
    error: null
  },
  addScene: {
    chosenItem: '',
    chosenProfile: '',
  }
}