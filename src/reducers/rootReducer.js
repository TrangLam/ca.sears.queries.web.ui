import { combineReducers } from 'redux';
import {
  tasksList,
  taskComponents,
  deleteModal,
  taskToDelete,
  submitForm,
  addScene
} from './taskReducer';
import { authentication } from './authReducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  authentication,
  tasksList,
  taskComponents,
  taskToDelete,
  deleteModal,
  submitForm,
  addScene,
  form: formReducer
});

export default rootReducer;