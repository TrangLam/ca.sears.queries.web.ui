// React and Redux
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Switch, withRouter } from 'react-router-dom';

// Import custom components and functions
import * as authAction from './actions/authActions';
import Home from './scenes/Home';
import Layout from './components/Basics/Layout';
import EditTask from './containers/EditTask';
// import EditScene from './scenes/EditScene';
import AddScene from './scenes/AddScene';
import TasksListScene from './scenes/TasksListScene';

// Authorization
// import AuthService from './auth/authService';  // Lock 10 Implementation
import Auth from './auth/auth';
const auth = new Auth();

class App extends Component {
///////////////////////////////
// Lock 10 Implementation   //
/////////////////////////////

//   constructor(props) {
//     super(props);
//
//     this.authService = new AuthService();
//   }
//
//   componentWillMount() {
//     // 'authenticated' event
//     this.authService.lock.on('authenticated', (authResult) => {
//       this.authService.lock.getProfile(authResult.idToken, (error, profile) => {
//         if (error) {
//           this.props.actions.loginError(error);
//           return this.props.history.push({ pathname: '/' });
//         }
//
//         AuthService.setToken(authResult.idToken);
//         AuthService.setProfile(profile);
//         this.props.actions.loginSuccess();
//
//         // If login succeeds, redirect to Task List page
//         return this.props.history.push({ pathname: '/lists' });  // props history is provided by withRouter function
//       });
//     });
//
//     // 'authorization_error' event
//     this.authService.lock.on('authorization_error', (error) => {
//       this.props.actions.loginError(error);
//       return this.props.history.push({ pathname: '/' });  // props history is provided by withRouter function
//     })
//   }

  // Comment this out for Lock 10 Implementation
  componentWillMount() {
    // Need to check isAuthenticated because when the element <App/> is reloaded either by
    // refreshing the page, or opening a new tab, all states will be reset. If states are not set
    // App will render login page.
    if (Auth.isAuthenticated()) { // If is already logged in
      this.props.actions.loginSuccess(Auth.userCanEdit());
    } else if (/access_token|id_token|error/.test(this.props.history.location.hash)) {
      auth.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          Auth.setSession(authResult);
          this.props.actions.loginSuccess(Auth.userCanEdit());
          this.props.history.replace('/lists');
        } else if (err) {
          this.props.history.replace('/');
          this.props.actions.loginError(err.errorDescription);
          console.log('error here', err);
        }
      });
    }
  }

  render() {
    const routes = [
      {
        key: 'Home',
        path: '/',
        exact: true,
        component: Home,
      },
      {
        key: 'TasksListScene',
        path: '/lists',
        exact: true,
        component: TasksListScene,
      },
      {
        key: 'AddScene',
        path: '/add',
        exact: true,
        component: AddScene,
      },
      {
        key: 'EditTask',
        path: '/edit',
        exact: true,
        component: EditTask,
      }
    ];

    return (
      <div>
        <Switch>
          {routes.map((item) => {
            item.auth = {
              canEdit: this.props.canEdit,
              login: auth.login,
              logout: this.props.actions.logout,
              loggedIn: this.props.loggedIn,
              error: this.props.authError,
            };
            if (item.key === 'Home') {  // If Home, assign className homePage for styling
              item.className = 'home-page'
            }
            return Layout(item);
          })}
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    canEdit: state.authentication.canEdit,
    loggedIn: state.authentication.loggedIn,
    authError: state.authentication.error,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(authAction, dispatch)
  };
};

// Connect function subscribes the component to the store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));