// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Import custom components and functions
import FormComponent from '../components/Basics/FormComponent';
import Error from '../components/Basics/Error';
import * as taskActions from '../actions/taskActions';

class AddTaskProfile extends Component {
  constructor(props) {
    super(props);

    this.saveTaskProfile = this.saveTaskProfile.bind(this);
  }

  // Reset succeeded and error states
  // in SubmitForm store when component unmounts
  componentWillUnmount() {
    this.props.actions.resetSubmitFormState();
  }

  saveTaskProfile(values) {
    // const { name, ...rest } = values;
    // const data = {
    //   profile: this.props.profile,
    //   name: name,
    //   taskProperties: { ...rest }
    // };

    return this.props.actions.submitForm(data, 'add');
  }

  render() {
    const { submitForm } = this.props;
    const error = submitForm.error;

    const fields = [];

    if (this.props.submitForm.succeeded) {  // if submit successfully, redirect to Tasks page
      return <Redirect to="/lists"/>;
    } else {
      return (
        <div>
          <h3>Add New Task Profile</h3>
          <FormComponent
            fields={fields}
            handleSubmitFnc={this.saveTaskProfile}/>
          { error && <Error message={error}/>}
        </div>
      );
    }
  }
}

AddTaskProfile.propTypes = {
  submitForm: PropTypes.object.isRequired,
};

const mapStatesToProps = (state) => {
  return {
    submitForm: state.submitForm,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  }
};

// Connect function subscribes the component to the store
export default connect(mapStatesToProps, mapDispatchToProps)(AddTaskProfile);