// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Import custom components and functions
import FormComponent from '../components/Basics/FormComponent';
import Error from '../components/Basics/Error';
import * as taskActions from '../actions/taskActions';
import * as helpers from '../utils/helpers';

class EditTask extends Component {
  constructor(props) {
    super(props);

    this.saveTask = this.saveTask.bind(this);
  }

  // Reset succeeded and error states
  // in SubmitForm store when component unmounts
  componentWillUnmount() {
    this.props.actions.resetSubmitFormState();
  }

  saveTask(values) {
    const initialValues = this.props.location.state.task;
    // const initialValues = this.props.task;
    const diffValues = helpers.difference(values, initialValues);

    const submitValue = {
      id: values.id,
      changes: diffValues
    };

    return this.props.actions.submitForm(submitValue, 'edit');
  }

  render() {
    const error = this.props.submitForm.error;
    // const { task, fields } = this.props;
    const { task, fields }  = this.props.location.state;
    const initialValues = task ? task : { name: ''};

    if (this.props.submitForm.succeeded) {  // if submit successfully, redirect to Tasks page
      return <Redirect to="/lists"/>;
    } else {
      return (
        <div>
          <h3>Edit Task: {initialValues.name}</h3>
          <FormComponent
            name="editForm"
            initialValues={initialValues}
            fields={fields}
            handleSubmitFnc={this.saveTask}/>
          { error && <Error message={error}/>}
        </div>
      );
    }
  }
}

EditTask.propTypes = {
  submitForm: PropTypes.object.isRequired,
};

const mapStatesToProps = (state) => {
  return {
    submitForm: state.submitForm,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  }
};

// Connect function subscribes the component to the store
export default connect(mapStatesToProps, mapDispatchToProps)(EditTask);