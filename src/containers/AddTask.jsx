// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Import custom components and functions
import FormComponent from '../components/Basics/FormComponent';
import Error from '../components/Basics/Error';
import * as taskActions from '../actions/taskActions';

class AddTask extends Component {
  constructor(props) {
    super(props);

    this.saveTask = this.saveTask.bind(this);
  }

  // Reset succeeded and error states
  // in SubmitForm store when component unmounts
  componentWillUnmount() {
    this.props.actions.resetSubmitFormState();
  }

  saveTask(values) {
    const { name, ...rest } = values;
    const data = {
      profile: this.props.profile,
      name: name,
      taskProperties: { ...rest }
    };

    return this.props.actions.submitForm(data, 'add');
  }

  render() {
    const { fields, submitForm, profile } = this.props;
    const error = submitForm.error;

    // Add name field
    fields.push({
      label: "name",
      type: "input",
      required: true,
      allowEdit: true // only editable when adding
    });

    if (this.props.submitForm.succeeded) {  // if submit successfully, redirect to Tasks page
      return <Redirect to="/lists"/>;
    } else {
      return (
        <div>
          <h3>Add New Task</h3>
          <FormComponent
            key={profile}
            name={profile}
            fields={fields}
            handleSubmitFnc={this.saveTask}/>
          { error && <Error message={error}/>}
        </div>
      );
    }
  }
}

AddTask.propTypes = {
  submitForm: PropTypes.object.isRequired,
};

const mapStatesToProps = (state) => {
  return {
    submitForm: state.submitForm,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  }
};

// Connect function subscribes the component to the store
export default connect(mapStatesToProps, mapDispatchToProps)(AddTask);