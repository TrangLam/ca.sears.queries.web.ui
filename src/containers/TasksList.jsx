// Util libraries
import keys from 'lodash/keys';

// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Import custom components and functions
import * as taskActions from '../actions/taskActions';
import Error from '../components/Basics/Error';
import TasksTable from '../components/Task/TasksTable';
import DeleteTask from './DeleteTask';

class TasksList extends Component {
  constructor(props) {
    super(props);

    // Bind functions to this
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentWillMount() {
    // Load all task components
    this.props.actions.loadAllTaskComponents();
    // Load all tasks
    this.props.actions.loadTasks('all');
  }

  /**
   * Dispatch action to toggle deleteModal show state
   */
  handleDelete(task) {
    this.props.actions.selectToDelete(task);
    this.props.actions.toggleDeleteModal();
  }

  render() {
    const { allTasks, taskComponents, canEdit, tasksListError } = this.props;
    const profiles = keys(allTasks);
    let noDataMessage = 'There is no data to display.';
    return (
      <div>
        { tasksListError && <Error message={tasksListError} />}
        <DeleteTask />
        <h2>DEBI Tasks</h2>
        {
          profiles.length > 0 &&
          profiles.reduce((memo, profile) => {
            const tasks = allTasks[profile];

            if (tasks.length > 0) {
              noDataMessage='';
              memo.push(
                <TasksTable
                  key={profile}
                  profile={profile}
                  canEdit={canEdit}
                  tasks={tasks}
                  fields={taskComponents}
                  handleDelete={this.handleDelete}
                />
              );
            }
            return memo;
          }, [])
        }
        <div>{noDataMessage}</div>
      </div>
    );
  }
}

TasksList.propTypes = {
  allTasks: PropTypes.object.isRequired,
  taskComponents: PropTypes.object.isRequired,
  canEdit: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => {
  return {
    allTasks: state.tasksList.tasks,
    tasksListError: state.tasksList.error,
    taskComponents: state.taskComponents,
    canEdit: state.authentication.canEdit,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TasksList);