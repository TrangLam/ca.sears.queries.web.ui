// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Custom components and functions
import * as taskActions from '../actions/taskActions';
import ModalComponent from '../components/Basics/ModalComponent';

class DeleteTask extends Component {
  constructor(props) {
    super(props);

    // Bind functions to this
    this.handleCancel = this.handleCancel.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleCancel() {
    this.props.actions.clearTaskToDelete();
    this.props.actions.toggleDeleteModal();
  }

  handleDelete() {
    this.props.actions.deleteTask(this.props.taskToDelete);
  }

  render() {
    // Get name
    const taskName = this.props.taskToDelete.name;

    // modalTitle
    const title = `Deleting Task`;

    // modalBody
    const body = (
      <div>
        Are you sure you want to delete the following task?
        <ul><li>{taskName}</li></ul>
      </div>
    );

    // proceedText
    const text = `DELETE TASK`;

    return (
      <ModalComponent
        onCancel={ this.handleCancel }
        onProceed={ this.handleDelete }
        show={ this.props.showDeleteModal }
        error={ this.props.errorDeleteModal }
        modalTitle={ title }
        modalBody={ body }
        proceedText={ text } />
    );
  }
}

DeleteTask.propTypes = {
  taskToDelete: PropTypes.object.isRequired,
  showDeleteModal: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => {
  return {
    taskToDelete: state.taskToDelete,
    showDeleteModal: state.deleteModal.show,
    errorDeleteModal: state.deleteModal.error
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DeleteTask);