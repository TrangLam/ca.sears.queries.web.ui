import request from 'request-promise';
import config from '../config';
import Auth from '../auth/auth';
const filename = 'tasksApi';

const baseRequest = (options, fnName) => {
  return new Promise((resolve, reject) => {
    request(options)
      .then((result) => {
        console.log(`Success: ${filename} ${fnName}`, result);
        resolve(result);
      })
      .catch((error) => {
        console.log(`Error: ${filename} ${fnName}`, error);
        reject(error);
      })
  });
};

const Api = {
  /**
   * Get all task components categorized by task profile
   */
  getAllTaskComponents() {
    const options = {
      method: 'GET',
      uri: `${config.dbUrl}/components`,
      headers: {
        'Authorization': `Bearer ${Auth.getAccessToken()}`
      },
      json: true
    };

    return baseRequest(options, 'getAllTaskComponents');
  },

  /**
   * Get tasks based on provided profile
   * Profile can be 'all', or a specific task profile
   *
   * @param profile
   */
  getTasks(profile) {
    const options = {
      method: 'GET',
      uri: `${config.dbUrl}/tasks/${profile}`,
      headers: {
        'Authorization': `Bearer ${Auth.getAccessToken()}`
      },
      json: true
    };

    return baseRequest(options, 'getTasks');
  },

  /**
   * Delete task with id provided
   *
   * @param data
   */
  deleteTasks(data) {
    const options = {
      method: 'DELETE',
      uri: `${config.dbUrl}/tasks`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Auth.getAccessToken()}`
      },
      body: data,
      json: true
    };

    return baseRequest(options, 'deleteTasks');
  },

  editTask(data) {
    const options = {
      method: 'PUT',
      uri: `${config.dbUrl}/tasks/${data.id}`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Auth.getAccessToken()}`
      },
      body: data.changes,
      json: true
    };

    return baseRequest(options, 'editTask');
  },

  addTask(data) {
    const options = {
      method: 'POST',
      uri: `${config.dbUrl}/tasks`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Auth.getAccessToken()}`
      },
      body: data,
      json: true
    };

    return baseRequest(options, 'addTask');
  }

};

export default Api;