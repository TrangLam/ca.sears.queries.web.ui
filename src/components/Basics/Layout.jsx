import React from 'react';
import { Route } from 'react-router-dom';
import { Col } from 'react-bootstrap';
import NavigationBar from './NavigationBar';
import Error from './Error';

const Layout = ({ component: Component, auth, className, ...rest}) => {
  return (
    <Route {...rest} render={(matchProps) => {
      return (
        <Col xs={12} sm={12} className={className}>
          <NavigationBar
            canEdit={auth.canEdit}
            login={auth.login}
            logout={auth.logout}
            loggedIn={auth.loggedIn}/>
          <div className="bodyContent">
            <Component {...matchProps} login={auth.login}/>
            {auth.error && <Error message={auth.error} />}
          </div>
        </Col>
      );
    }} />
  );
};

export default Layout;