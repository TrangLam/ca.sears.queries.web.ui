import React from 'react';
import {
  FormControl,
  ControlLabel,
  Col
} from 'react-bootstrap';

const DropDownList = (props) => {
  const { options, title, label, handleClick } = props;
  return (
    <div className="drop-down-list">
      <Col sm={12}>
        <Col componentClass={ControlLabel} sm={3}>{label}</Col>
        <Col sm={6}>
          <FormControl
            componentClass="select"
            onChange={handleClick}
            placeholder={title}>
            <option value="" key="default">Choose a Task Profile</option>
            {
              options.map((option) => {
                return (
                  <option
                    value={option}
                    key={option}>{option}</option>
                );
              })
            }
          </FormControl>
        </Col>
      </Col>
    </div>
  );
};

export default DropDownList;