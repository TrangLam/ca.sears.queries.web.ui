import React from 'react';
import {
  FormGroup,
  ControlLabel,
  Col,
  Radio
} from 'react-bootstrap';

const RadioButtonGroup = (props) => {
  const { label, options, radioName, handleClick } = props;
  // For each option in props.options, create a Radio button
  return (
    <div className="radio-form">
      <Col sm={12}>
        <Col componentClass={ControlLabel} sm={3}>{label}</Col>
        <Col sm={6}>
          <FormGroup>
            {
              options.map((option) => {
                return (
                  <Radio
                    inline
                    key={option.label}
                    name={radioName}
                    onClick={() => handleClick(option.value)} >
                    {option.label}
                  </Radio>
                );
              })
            }
          </FormGroup>
        </Col>
      </Col>
    </div>
  );
};

export default RadioButtonGroup;