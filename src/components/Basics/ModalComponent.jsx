import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import '../../index.css';
import Error from './Error';

const ModalComponent = (props) => {
  const modalTitle = props.modalTitle;
  const modalBody = props.modalBody;
  const proceedText = props.proceedText;
  return (
    <div className="modal-component">
      <Modal show={ props.show }>
        <Modal.Header>
          <Modal.Title>{ modalTitle }</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          { modalBody }
          {props.error && <Error message={ props.error } />}
        </Modal.Body>
        <Modal.Footer>
          <Button
            className="modal-button primary-button"
            onClick={ props.onCancel }>CANCEL</Button>
          <Button
            className="modal-button"
            onClick={ props.onProceed }>{ proceedText }</Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ModalComponent;