import React from 'react';
import {
  Button,
  Col,
  Form,
  Glyphicon
} from 'react-bootstrap';
import {
  Field,
  FieldArray,
  reduxForm
} from 'redux-form';
import RenderField from './RenderField';

const fieldInfoGroup = (field, index, fields) => (
  <Col key={index} sm={12} className="growing-field">
    <Col sm={11}>
      <Field
        name={`${field}.label`}
        type="text"
        component={RenderField}
        label="Field Label"/>
      <Field
        name={`${field}.type`}
        type="text"
        component={RenderField}
        label="Field Type"/>
      <Field
        name={`${field}.required`}
        type="text"
        component={RenderField}
        label="Required"/>
      <Field
        name={`${field}.allowEdit`}
        type="text"
        component={RenderField}
        label="Allow Editing"/>
    </Col>
    <Col sm={1}>
      <Button
        className="naked-button"
        type="button"
        onClick={() => fields.remove(index)}>
        <Glyphicon glyph="glyphicon glyphicon-minus-sign" />
      </Button>
    </Col>
  </Col>
);

const FieldInfoGroups = ({ fields }) => (
  <div>
    {fields.map(fieldInfoGroup)}
    <Button className="form-button" type="button" onClick={() => fields.push({})}>
      Add New Field
    </Button>
  </div>
);

let GrowingForm = (props) => {
  const { name } = props;

  return (
    <Form horizontal>
      <FieldArray
        name={name}
        component={FieldInfoGroups}
      />
    </Form>
  );
};

GrowingForm = reduxForm({
  form: 'growingForm',
})(GrowingForm);

export default GrowingForm;