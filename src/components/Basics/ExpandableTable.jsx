import React from 'react';
import PropTypes from 'prop-types';
import {
  BootstrapTable,
  TableHeaderColumn
} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

const ExpandableTable = (props) => {
  const { data, expandComponent, expandableRow } = props;
  const headers = data.length > 0 ? Object.keys(data[0]) : null;

  const tdStyle = {
    // whiteSpace: 'normal' // todo - remove
  };

  return (
    <BootstrapTable
      className="custom-table"
      data={data}
      expandableRow={expandableRow}
      expandComponent={expandComponent}
      striped hover bordered={false}>
      {
        !headers &&
        <TableHeaderColumn
          isKey={true}
          tdStyle={tdStyle}
          dataField="...">Loading</TableHeaderColumn>
      }
      {
        headers &&
        headers.map((header) => {
          return (
            <TableHeaderColumn
              isKey={header==='id'}
              key={header}
              tdStyle={tdStyle}
              dataField={header}>{header}</TableHeaderColumn>
          );
        })
      }
    </BootstrapTable>
  );
};

ExpandableTable.propTypes = {
  data: PropTypes.array.isRequired
};

export default ExpandableTable;