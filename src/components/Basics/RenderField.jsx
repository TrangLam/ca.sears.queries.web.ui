import React from 'react';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Col
} from 'react-bootstrap';

/**
 * Return 'success' if field is filled
 * If field is touched and error, return error
 *
 * @param fieldValidation
 * @return {*}
 */
const getValidationState = (touched, error) => {
  if (!touched) {
    return null;
  }
  else {
    return error ? 'error' : 'success';
  }
};

const RenderField = (props) => {
  const {
    input,
    label,
    type,
    placeholder,
    meta,
    noValidation,
    isDisabled,
    componentClass } = props;

  return (
    <FormGroup
      controlId={input.name}
      validationState={noValidation ? null : getValidationState(meta.touched, meta.error)}>
      <Col sm={12}>
        <Col componentClass={ControlLabel} sm={2}>{ label }</Col>
        <Col sm={8}>
          <FormControl
            {...input}
            disabled={isDisabled}
            type={type}
            placeholder={placeholder}
            componentClass={componentClass} />
        </Col>
      </Col>
    </FormGroup>
  );
};


export default RenderField;