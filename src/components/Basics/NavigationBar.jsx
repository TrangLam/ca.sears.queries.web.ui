import React, { Component } from 'react';
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap';
import { Nav, NavItem, Navbar, NavbarBrand } from 'react-bootstrap';
import { bootstrapUtils } from 'react-bootstrap/lib/utils';
import '../../index.css';

// Allow Nav to have custom style class
bootstrapUtils.addStyle(Nav, 'custom');

export default class NavigationBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeKey : 1
    };

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  login() {
    this.props.login();
  }

  logout() {
    this.props.logout();
  }

  handleSelect(selectedKey) {
    this.setState({
      activeKey : selectedKey
    });
  }

  render() {
    const loggedIn = this.props.loggedIn;
    const canEdit = this.props.canEdit;

    return (
      <Navbar fixedTop>
        <NavbarBrand>
          <img id="nav-logo" src="./images/debi_logo.png" alt="debi logo"/>
        </NavbarBrand>
        <Nav
          bsStyle="pills custom navbar-right"
          activeKey={this.state.activeKey}
          onSelect={this.handleSelect}>
          {loggedIn && (
            <IndexLinkContainer to="/lists">
              <NavItem eventKey={1}>TASKS</NavItem>
            </IndexLinkContainer>
          )}
          {loggedIn && canEdit && (
            <LinkContainer to="/add">
              <NavItem eventKey={2}>ADD</NavItem>
            </LinkContainer>
          )}
          {loggedIn && (
            <NavItem onSelect={this.logout}>LOG OUT</NavItem>
          )}
          {!loggedIn && (
            <NavItem onSelect={this.login}>LOG IN</NavItem>
          )}
        </Nav>
      </Navbar>
    );
  }
}