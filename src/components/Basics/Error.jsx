import React from 'react';

const Error = (props) => {
  return (
    <div className="error-box">
      ERROR: {props.message}
    </div>
  );
};

export default Error;