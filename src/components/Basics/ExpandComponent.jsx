import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Table,
  Glyphicon
} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const ExpandComponent = (props) => {
  let {
    fields,
    data,
    canEdit,
    handleDelete } = props;

  const toObject = {
    pathname: "/edit",
    state: {
      task: data,
      fields,
    }
  };

  return (
    <div>
      <div>
        {
          canEdit &&
          (<div className="buttons-group">
            <Button
              className="naked-button"
              onClick={handleDelete}>
              <Glyphicon glyph="glyphicon glyphicon-trash" />
            </Button>
            <Link to={toObject}>
              <Button className="naked-button">
                <Glyphicon glyph="glyphicon glyphicon-pencil" />
              </Button>
            </Link>
          </div>)
        }
        <h3>Task: {data.name}</h3>
      </div>
      <Table bsClass="expand-component">
        <tbody>
        {
          fields.length > 0 &&
          fields.map((field) => {
            const {label} = field;

            return (
              <tr key={label}>
                <td width="10%" className="expand-component-label">{label}</td>
                <td width="90%" style={{whiteSpace: 'normal'}}>
                  {data[label]}
                </td>
              </tr>
            );
          })
        }
        </tbody>
      </Table>
    </div>
  );
};

ExpandComponent.propTypes = {
  fields: PropTypes.array.isRequired,
  data: PropTypes.object.isRequired,
  canEdit: PropTypes.bool.isRequired,
  handleDelete: PropTypes.func.isRequired
};

export default ExpandComponent;