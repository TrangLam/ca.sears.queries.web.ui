import React from 'react';
import {
  Form,
  FormGroup,
  Col,
  Button
} from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import RenderField from './RenderField';
import '../../index.css';

/**
 * Return the appropriate button text
 * according to param saving
 *
 * @param saving
 * @return {*}
 */
const getButtonText = (saving) => {
  if (saving)
    return 'SAVING';
  else
    return 'SAVE';
};

const requiredFnc = (value) => (value ? undefined : 'Required');

let FormComponent = (props) => {
  let {
    fields,
    name,
    handleSubmit,
    handleSubmitFnc,
    submitting } = props;

  return (
    <Form
      horizontal
      name={name}
      onSubmit={handleSubmit(handleSubmitFnc)}>
      {
        fields.length > 0 &&
        fields.map((field) => {
          const {
            label,
            type,
            required,
            allowEdit,
          } = field;
          const key = (props.initialValues ? label : `${name}-${label}`);

          return (
            <Field
              key={key}
              type={type}
              componentClass={type}
              name={label}
              label={`${label}${(required ? `*` : ``)}`}
              component={RenderField}
              isDisabled={!allowEdit}
              noValidation={!required}
              validate={required ? requiredFnc : () => undefined}
            />
          );
        })
      }
      <FormGroup className="form-button-group">
        <Col sm={10}>
          <Button
            className="form-button"
            type="submit"
            disabled={submitting}>
            {getButtonText(submitting)}
          </Button>
        </Col>
      </FormGroup>
    </Form>
  );
};

FormComponent = reduxForm({
  form: 'formComponent',
  enableReinitialize: true
})(FormComponent);

export default FormComponent;