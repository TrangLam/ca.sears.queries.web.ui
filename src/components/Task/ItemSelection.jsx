import React from 'react';
import RadioButtonGroup from '../Basics/RadioButtonGroup';

const ItemSelection = (props) => {
  const itemOptions = [
    {
      label: 'Task',
      value: 'task'
    },
    {
      label: 'Task Profile',
      value: 'profile'
    }
  ];

  return (
    <RadioButtonGroup
      label="What do you want to add?"
      options={itemOptions}
      radioName="chooseItem"
      handleClick={props.handleClick} />
  );
};

export default ItemSelection;