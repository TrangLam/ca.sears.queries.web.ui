// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Import custom components and functions
import ExpandableTable from '../Basics/ExpandableTable';
import ExpandComponent from '../Basics/ExpandComponent';

class TasksTable extends Component {
  constructor(props) {
    super(props);

    this.expandComponent = this.expandComponent.bind(this);
    this.isExpandable = this.isExpandable.bind(this);
  }

  expandComponent(row) {
    const { fields, profile, canEdit, handleDelete } = this.props;

    return (
      <ExpandComponent
        data={row}
        fields={fields[profile]}
        canEdit={canEdit}
        handleDelete={() => handleDelete(row)}/>
    );
  };

  isExpandable() { // All row is expandable
    return true;
  };

  render() {
    const { tasks } = this.props;

    return (
      <div className="tasks-table">
        <h3>{`${this.props.profile} Tasks`}</h3>
        <ExpandableTable
          data={tasks}
          expandComponent={this.expandComponent}
          expandableRow={this.isExpandable}
        />
      </div>
    );
  }
};

TasksTable.propTypes = {
  tasks: PropTypes.array.isRequired,
  fields: PropTypes.object.isRequired,
  canEdit: PropTypes.bool.isRequired,
  profile: PropTypes.string.isRequired,
  handleDelete: PropTypes.func.isRequired
};

export default TasksTable;