import React from 'react';
import DropDownList from '../Basics/DropDownList';

const TaskProfileSelection = (props) => {
  const { options, handleClick } = props;
  return (
    <DropDownList
      label="What is the new task's profile?"
      title="Chose Task Profile"
      options={options}
      handleClick={handleClick} />
  );
};

export default TaskProfileSelection;