import config from '../config';

export const AUTH_CONFIG = {
  domain: 'debi-sears.auth0.com',
  clientID: 'jTiDRyk8Q35Ddd5iTFHWlUH8OMJka1zK',
  redirectUri: config.currentUrl,
  audience: '52.207.254.166',
  responseType: 'token id_token',
  scope: 'read:allqueries delete:queries add:query edit:query',
};

export const LOCK_CONFIG = {
  title: 'Tasks Management',
  logo: `${config.currentUrl}/images/debi_logo.png`,
  // connection: 'google-oauth2'
};