// Util libraries
import keys from 'lodash/keys';

// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';

// Import custom components and functions
import * as taskActions from '../actions/taskActions';
// import Error from '../components/Basics/Error';
import ItemSelection from '../components/Task/ItemSelection';
import TaskProfileSelection from '../components/Task/TaskProfileSelection';
import AddTask from '../containers/AddTask';
import GrowingForm from '../components/Basics/GrowingForm';

class AddScene extends Component {
  constructor(props) {
    super(props);

    this.handleChooseItem = this.handleChooseItem.bind(this);
    this.handleChooseProfile = this.handleChooseProfile.bind(this);
  }

  componentWillMount() {
    // Load all task components
    if (this.props.loggedIn) {
      this.props.actions.loadAllTaskComponents();
    }
  }

  componentWillUnmount() {
    this.props.actions.clearAddScene(); // Clear values of chosenItem and chosenProfile
  }

  handleChooseItem(value) {
    if (value === "profile" && this.props.chosenProfile) {
        this.props.actions.clearProfile();
    }
    return this.props.actions.chooseItem(value);
  }

  handleChooseProfile(event) {
    this.props.actions.chooseProfile(event.target.value);
  }

  render() {
    if (!this.props.loggedIn) {
      return <Redirect to="/" />;
    } else {
      const { taskComponents, chosenItem, chosenProfile } = this.props;
      const fields = taskComponents[chosenProfile];
      const profileOptions = keys(taskComponents);
      return (
        <div>
          <ItemSelection handleClick={this.handleChooseItem} />
          {
            chosenItem === "task" &&
              <div>
                <TaskProfileSelection
                  handleClick={this.handleChooseProfile}
                  options={profileOptions} />
                {
                  chosenProfile !== "" &&
                  <AddTask
                    key={chosenProfile}
                    fields={fields}
                    profile={chosenProfile} />
                }
              </div>
          }
          {
            chosenItem === "profile" &&
              <div>
                <h3>Add New Task Profile</h3>
                <GrowingForm name="AddNewProfile" />
              </div>
          }

        </div>
      );
    }
  }
}

AddScene.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  taskComponents: PropTypes.object.isRequired,
  chosenItem: PropTypes.string.isRequired,
  chosenProfile: PropTypes.string.isRequired
};

const mapStateToProps = (state) => {
  return {
    loggedIn: state.authentication.loggedIn,  // future refactor: this prop can just be passed down from LayOut
    taskComponents: state.taskComponents,
    chosenItem: state.addScene.chosenItem,
    chosenProfile: state.addScene.chosenProfile
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  };
};

// Connect function subscribes the component to the store
export default connect(mapStateToProps, mapDispatchToProps)(AddScene);