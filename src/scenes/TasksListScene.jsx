// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Import custom components and functions
import TasksList from '../containers/TasksList';

class TasksListScene extends Component {
  render() {
    const { loggedIn } = this.props;

    if (!loggedIn) {
      return <Redirect to="/" />;
    } else {
      return (
        <TasksList />
      );
    }
  }
}

TasksListScene.propTypes = {
  loggedIn: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => {
  return {
    loggedIn: state.authentication.loggedIn,  // future refactor: this prop can just be passed down from LayOut
  };
};

// Connect function subscribes the component to the store
export default connect(mapStateToProps)(TasksListScene);