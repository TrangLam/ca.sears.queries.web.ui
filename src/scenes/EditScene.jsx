// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// Import custom components and functions
import EditTask from '../containers/EditTask';

class AddScene extends Component {
  render() {
    const { loggedIn } = this.props;

    if (!loggedIn) {
      return <Redirect to="/" />;
    } else {
      const { task, fields }  = this.props.location.state;

      return (
        <EditTask task={task} fields={fields} />
      );
    }
  }
}

AddScene.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
  return {
    loggedIn: state.authentication.loggedIn,  // future refactor: this prop can just be passed down from LayOut
  };
};

// Connect function subscribes the component to the store
export default connect(mapStateToProps)(AddScene);