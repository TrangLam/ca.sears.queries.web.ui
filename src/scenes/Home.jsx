// React and Redux
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';

class Home extends Component {
  render() {
    return (
      <div className="home-content">
        <div id="team-name">debi</div>
        <h1>TASKS MANAGEMENT TOOL</h1>
        {
          !this.props.loggedIn &&
          <Button
            className="login-button"
            type="button"
            onClick={ this.props.login }>LOG IN</Button>
        }
      </div>
    );
  }
};

Home.propTypes = {
  loggedIn: PropTypes.bool.isRequired // future refactor: this prop can just be passed down from LayOut
};

const mapStateToProps = (state) => {
  return {
    loggedIn: state.authentication.loggedIn,
  };
};

// Connect function subscribes the component to the store
export default connect(mapStateToProps)(Home);