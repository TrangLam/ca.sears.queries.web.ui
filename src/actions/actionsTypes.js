// Authentication actions
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

// Load tasks actions
export const LOAD_TASKS_SUCCESS = 'LOAD_TASKS_SUCCESS';
export const LOAD_TASKS_FAILURE = 'LOAD_TASKS_FAILURE';

// Load task components actions
export const LOAD_TASK_COMPONENTS_SUCCESS = 'LOAD_TASK_COMPONENTS_SUCCESS';
export const LOAD_TASK_COMPONENTS_FAILURE = 'LOAD_TASK_COMPONENTS_FAILURE';

// Delete actions
export const TOGGLE_DELETE_MODAL = 'TOGGLE_DELETE_MODAL';
export const CLEAR_TASK_TO_DELETE = 'CLEAR_TASK_TO_DELETE';
export const SELECT_TO_DELETE = 'SELECT_TO_DELETE';
export const DELETE_TASKS_FAILURE = 'DELETE_TASKS_FAILURE';

// Edit/Add actions (Redux-form submit action)
export const SUBMIT_FORM_SUCCESS = 'SUBMIT_FORM_SUCCESS';
export const SUBMIT_FORM_FAILURE = 'SUBMIT_FORM_FAILURE';
export const RESET_SUBMIT_FORM = 'RESET_SUBMIT_FORM';

// Select options in Add Scene actions
export const CHOOSE_ITEM = 'CHOOSE_ITEM';
export const CHOOSE_PROFILE = 'CHOOSE_PROFILE';
export const CLEAR_ADD_SCENE = 'CLEAR_ADD_SCENE';
export const CLEAR_PROFILE = 'CLEAR_PROFILE';