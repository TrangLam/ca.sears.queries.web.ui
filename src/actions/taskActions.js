// import map from 'lodash/map';
import Api from '../api/tasksApi';
import * as types from './actionsTypes';

/*
 * LOAD TASKS ACTIONS
 */
export const loadTasks = (profile) => {
  return (dispatch) => {
    return Api.getTasks(profile)
      .then(tasks => {
        dispatch(loadTasksSuccess(tasks));
      })
      .catch(error => {
        dispatch(loadTasksFailure(error));
      })
  }
};

export const loadTasksSuccess = (tasks) => {
  return {
    type: types.LOAD_TASKS_SUCCESS,
    tasks
  }
};

export const loadTasksFailure = (error) => {
  return {
    type: types.LOAD_TASKS_FAILURE,
    error
  }
};

/*
 * LOAD TASK COMPONENTS
 */
export const loadAllTaskComponents = () => {
  return (dispatch) => {
    return Api.getAllTaskComponents()
      .then(taskComponents => {
        dispatch(loadAllTaskComponentsSuccess(taskComponents));
      })
      .catch(error => {
        dispatch(loadAllTaskComponentsFailure(error));
      })
  }
};

export const loadAllTaskComponentsSuccess = (taskComponents) => {
  return {
    type: types.LOAD_TASK_COMPONENTS_SUCCESS,
    taskComponents
  }
};

export const loadAllTaskComponentsFailure = (error) => {
  return {
    type: types.LOAD_TASK_COMPONENTS_FAILURE,
    error
  }
};

/*
 * DELETE ACTIONS
 */
export const toggleDeleteModal = () => {
  return {
    type: types.TOGGLE_DELETE_MODAL,
  }
};

export const selectToDelete = (task) => {
  return {
    type: types.SELECT_TO_DELETE,
    task
  };
};

export const clearTaskToDelete = () => {
  return {
    type: types.CLEAR_TASK_TO_DELETE
  }
};

/**
 * Delete one task
 *
 * @param task
 * @return {function(*)}
 */
export const deleteTask = (task) => {
  return (dispatch) => {
    const ids = {
      ids: [task.id]  // API call is designed to handle one or more tasks
    };

    return Api.deleteTasks(ids)
      .then(() => {
        dispatch(deleteTasksSuccess());
      })
      .catch(error => {
        dispatch(deleteTasksFailure(error));
      })
  }
};

/**
 * When tasks are deleted successfully, the app must:
 * - Reload Task List
 * - Close Delete Modal
 *
 * @return {function(*)}
 */
export const deleteTasksSuccess = () => {
  return (dispatch) => {
    dispatch(clearTaskToDelete());
    dispatch(loadTasks('all'));
    dispatch(toggleDeleteModal());
  };
};

export const deleteTasksFailure = (error) => {
  return {
    type: types.DELETE_TASKS_FAILURE,
    error
  }
};

/*
 * EDIT/ADD ACTIONS [Redux-form submit actions]
 */
export const submitForm = (task, method) => {
  let apiMethod;
  if (method === 'add') {
    apiMethod = Api.addTask;
  } else {
    apiMethod = Api.editTask;
  }

  return (dispatch) => {
    return apiMethod(task)
      .then(() => {
        dispatch(submitFormSuccess());
      })
      .catch(error => {
        dispatch(submitFormFailure(error));
      })
  }
};

export const submitFormSuccess = () => {
  return {
    type: types.SUBMIT_FORM_SUCCESS
  }
};

export const submitFormFailure = (error) => {
  return {
    type: types.SUBMIT_FORM_FAILURE,
    error
  }
};

export const resetSubmitFormState = () => {
  return {
    type: types.RESET_SUBMIT_FORM
  }
};

/*
 * ADD SCENE ACTIONS
 */
export const chooseItem = (chosenItem) => {
  return {
    type: types.CHOOSE_ITEM,
    chosenItem
  }
};

export const chooseProfile = (chosenProfile) => {
  return {
    type: types.CHOOSE_PROFILE,
    chosenProfile
  }
};

export const clearAddScene = () => {
  return {
    type: types.CLEAR_ADD_SCENE
  }
}

export const clearProfile = () => {
  return {
    type: types.CLEAR_PROFILE
  }
};