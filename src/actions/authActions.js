import * as types from './actionsTypes';
// import AuthService from '../auth/authService';
import Auth from '../auth/auth';

export const loginSuccess = (canEdit) => {
  return {
    type: types.LOGIN_SUCCESS,
    canEdit
  }
};

export const loginError = (error) => {
  return {
    type: types.LOGIN_ERROR,
    error
  }
};

export const logoutSuccess = () => {
  return {
    type: types.LOGOUT_SUCCESS
  }
};

// export const logoutError = (error) => {
//   return {
//     type: types.LOGOUT_ERROR,
//     error
//   }
// };

export const logout = () => {
  return (dispatch) => {
    Auth.logout();
    dispatch(logoutSuccess());
  };
};

export const checkLoggedIn = () => {

};